// My Carousel Function
let carouselMainFrame = document.querySelector("#carouselMainFrame");
let slides = document.getElementsByClassName("carousel-slide");
let carouselPreviousButton = document.querySelector("#carouselPreviousButton");
let carouselNextButton = document.querySelector("#carouselNextButton");
let firstPosition = 0;
let secondPosition = 1;
// to set the position of slides in order from top to bottom
let zIndexPos = slides.length, slide;
for ( slide of slides) {
	slide.style.zIndex = zIndexPos.toString();
	zIndexPos--;
}
//carousel animation forward
const carouselSlide = () => {
	if(slides.length > 1) {
		slides[firstPosition].style.animationName = "presentSlideForward";
		slides[secondPosition].style.animationName = "nextSlideForward";
		if (firstPosition == slides.length - 1) {
			firstPosition = 0;
			secondPosition++;
		} else if (secondPosition == slides.length - 1) {
			secondPosition = 0;
			firstPosition++;
		} else {
			secondPosition++;
			firstPosition++;
		}
	}
}
//carousel animation backward
const carouselSlideReverse = () => {
	if(slides.length > 1) {
		if (firstPosition == 0) {
			firstPosition = slides.length - 1;
			secondPosition--;
		} else if (secondPosition == 0) {
			secondPosition = slides.length - 1;
			firstPosition--;
		} else {
			secondPosition--;
			firstPosition--;
		}
		slides[secondPosition].style.animationName = "presentSlideBackward";
		slides[firstPosition].style.animationName = "previousSlideBackward";	
	}
}
//carousel animation interval
let slideInterval = setInterval(carouselSlide, 30000);


carouselMainFrame.onmouseenter = () => {
	clearInterval(slideInterval);
}

carouselMainFrame.onmouseleave = () => {
	slideInterval = setInterval(carouselSlide, 30000);
}

carouselPreviousButton.onclick = () => {
	carouselSlideReverse();
}

carouselNextButton.onclick = () => {
	carouselSlide();
}