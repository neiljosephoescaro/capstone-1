let introText = document.getElementById("introText");

let hoverMessage = document.getElementById("hoverMessage");

let fbLink = document.getElementById("fbLink"),
	liLink = document.getElementById("liLink"),
	myProjectsLink = document.getElementById("myProjectsLink"),
	myInfoLink = document.getElementById("myInfoLink"),
	myContactsLink = document.getElementById("myContactsLink");

let	limeText = document.getElementsByClassName("lime-text");

let main = document.getElementsByClassName("main"),
	sectionBackground = document.getElementById("sectionBackground"),
	closeButton = document.getElementById("closeButton");

let projectsCarousel = document.getElementById("carouselMainFrame");
let njoeProjectsTitle = document.getElementById("njoeProjectsTitle");

let infoTitle = document.getElementById("infoTitle");
let njoeInformation = document.getElementById("njoeInformation");
let contactsTitle = document.getElementById("contactsTitle");
let njoeContacts = document.getElementById("njoeContacts");

function typeWriter(text, className, index, speed, delay) {
	let classForTypeWriter, i, splitText, typeInterval;
	const typeFunction = () => {
		classForTypeWriter = document.getElementsByClassName(className);
		classForTypeWriter[index].innerHTML = "";
		splitText = text.split("");
		i = 0;
		const inputText = () => {
			if (i != splitText.length) {
				classForTypeWriter[index].innerHTML += splitText[i];	
			} else {
				clearInterval(typeInterval);
			}
			i++;
		}
		typeInterval = setInterval(inputText, speed);
	}
	setTimeout(typeFunction, delay);
}

typeWriter("const", "blue-text", 0 , 50, 7000);
typeWriter("njoe", "white-text", 0 , 50, 7300);
typeWriter("=", "red-text", 0 , 50, 7550);
typeWriter('"Neil Joseph O. Escaro"', "yellow-text", 0 , 50 , 7650);
typeWriter(";", "white-text", 1 , 50, 8850);
typeWriter("let", "blue-text", 1, 50 ,8950);
typeWriter("myFunction", "lime-text", 0, 50, 9150);
typeWriter("=", "red-text", 1, 50, 9700);
typeWriter("()", "white-text", 2, 50, 9800);
typeWriter("=>", "blue-text", 2, 50, 9950);
typeWriter("{", "white-text", 3, 50, 10150);
typeWriter("return", "red-text", 2, 50, 10250);
typeWriter("njoe", "white-text", 4, 50, 10600);
typeWriter("+", "red-text", 3, 50, 10850);
typeWriter('"is a computer technician and', "yellow-text", 1, 50, 10950);
typeWriter("\\", "purple-text", 0, 50, 12450);
typeWriter("web developer", "yellow-text", 2, 50, 12550);
typeWriter(";", "white-text", 5, 50, 13350);
typeWriter("}", " white-text", 6, 50, 13450);

fbLink.onmouseenter = () => {
	introText.style.display = "none";
	hoverMessage.style.display = "block";
	typeWriter("Add me on Facebook?", "lime-text", 1, 50, 0);
}

fbLink.onmouseleave = () => {
	hoverMessage.style.display = "none";
	introText.style.display = "block";
}

liLink.onmouseenter = () => {
	introText.style.display = "none";
	hoverMessage.style.display = "block";
	typeWriter("Go to my LinkedIn profile?", "lime-text", 1, 50, 0);
}

liLink.onmouseleave = () => {
	hoverMessage.style.display = "none";
	introText.style.display = "block";
	limeText[1].innerHTML = "";
}

myProjectsLink.onmouseenter = () => {
	introText.style.display = "none";
	hoverMessage.style.display = "block";
	typeWriter("Like to visit my projects?", "lime-text", 1, 50, 0);
}

myProjectsLink.onmouseleave = () => {
	hoverMessage.style.display = "none";
	introText.style.display = "block";
	limeText[1].innerHTML = "";
}

myInfoLink.onmouseenter = () => {
	introText.style.display = "none";
	hoverMessage.style.display = "block";
	typeWriter("Want to know my background?", "lime-text", 1, 50, 0);
}

myInfoLink.onmouseleave = () => {
	hoverMessage.style.display = "none";
	introText.style.display = "block";
	limeText[1].innerHTML = "";
}

myContactsLink.onmouseenter = () => {
	introText.style.display = "none";
	hoverMessage.style.display = "block";
	typeWriter("How to contact me?", "lime-text", 1, 50, 0);
}

myContactsLink.onmouseleave = () => {
	hoverMessage.style.display = "none";
	introText.style.display = "block";
	limeText[1].innerHTML = "";
}

myProjectsLink.onclick = () => {
	let a;
	for (a of main) {
		a.style.visibility = "hidden";
	}
	sectionBackground.style.display = "block";
	carouselMainFrame.style.display = "flex";
	njoeProjectsTitle.style.display = "block";
}

myInfoLink.onclick = () => {
	let a;
	for (a of main) {
		a.style.visibility = "hidden";
	}
	sectionBackground.style.display = "block";
	infoTitle.style.display = "block";
	njoeInformation.style.display = "block";
}

myContactsLink.onclick = () => {
	let a;
	for (a of main) {
		a.style.visibility = "hidden";
	}
	sectionBackground.style.display = "block";
	contactsTitle.style.display = "block";
	njoeContacts.style.display = "block";
}

closeButton.onclick = () => {
	let a;
	for (a of main) {
		a.style.visibility = "visible";
	}
	sectionBackground.style.display = "none";
	carouselMainFrame.style.display = "none";
	njoeProjectsTitle.style.display = "none";
	infoTitle.style.display = "none";
	njoeInformation.style.display = "none";
	contactsTitle.style.display = "none";
	njoeContacts.style.display = "none";
}


